//Autor: Curso Programación 2021
//Ubicación: Buenos Aires

/********************************
 *   DECLARACIÓN DE VARIABLES   *
 *   Y CONSTANTES               *
*********************************/

let nombre = "Pedro";
let edad = 37;
let _genero = "masculino";
let $apellido = "Ramirez"
let sueldo = 80000;
console.log(nombre, $apellido, _genero, edad, "sueldo: "+sueldo);